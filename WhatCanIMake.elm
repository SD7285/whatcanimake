module WhatCanIMake exposing (..)

import Html exposing (..)
import Html.Events exposing (onInput, onClick, onSubmit)
import Html.Events.Extra exposing (onEnter)
import Http
import Json.Decode exposing (string, int, float, list, field, map3, decodeString, Decoder)
import Json.Decode.Pipeline as PL exposing (decode, required, optional, requiredAt, optionalAt)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)

type FormField
  = FirstIng
  | SecondIng
  | ThirdIng

type alias Recipe =
    { recipeId : String
    , publisher : String
    , f2fUrl : String
    , title : String
    , sourceUrl : String
    , imageUrl : String
    , publisherUrl : String
    , socialRank : Float
    }

-- Model

type alias Model =
    { recipes : WebData (List Recipe)
    , firstIng : String
    , secondIng : String
    , thirdIng : String
    }

-- View

view : Model -> Html Msg
view model =
  div [generalContainerStyle]
      [ viewHeader
      , viewContent model
      , viewFooter
      ]

viewHeader : Html Msg
viewHeader =
 header [headerContainerStyle]
        [ h1 []
             [ text "What Can I Make?"]
        ]

viewContent : Model -> Html Msg
viewContent model =
 div []
      [ article [explanationContainerStyle]
                [ h1 []
                     [ text "Use Up Leftovers" ]
                , p  []
                     [ text "Choose up to 3 ingredients. We'll tell you what you can make!" ]
                ]
      , section [formContainerStyle]
                [ viewInputForm model ]
      , section [buttonContainerStyle]
                [ p []
                    [ button
                              [ type_ "submit"
                              , buttonStyle
                              , onClick SendHttpRequest
                              ]
                              [ text "What can I make?" ]
                    ]
                ]
      , section [resultContainerStyle]
                [ viewRecipesOrError model ]
      ]


viewInputForm : Model -> Html Msg
viewInputForm model =
        Html.form [ formStyle]
              [ fieldset [firstfieldSetStyle]
                [ legend []
                  [ text "1st Ingredient"
                  ]
                , input
                  [ id "first"
                  , type_ "text"
                  , onInput SetFirstIngredient
                  , value model.firstIng
                  ]
                  []
                ]
              , fieldset [secondfieldSetStyle]
                [ legend []
                  [ text "2nd Ingredient"
                  ]
                , input
                  [ id "second"
                  , type_ "text"
                  , onInput SetSecondIngredient
                  , value model.secondIng
                  ]
                  []
                ]
              , fieldset [thirdfieldSetStyle]
                [ legend []
                  [ text "3rd Ingredient"
                  ]
                , input
                  [ id "third"
                  , type_ "text"
                  , onInput SetThirdIngredient
                  , value model.thirdIng
                  ]
                  []
                ]
              ]

viewFooter : Html Msg
viewFooter =
 footer [footerContainerStyle]
        [ p []
            [ text "Contact: "
            , a [ href "sd7285@googlemail.com" ]
                [ text "sd7285@googlemail.com  " ]
            ]
        , p []
            [ text "  © 2019 by Steffen Dietrich " ]
        ]

viewRecipesOrError : Model -> Html Msg
viewRecipesOrError model =
    case model.recipes of
        RemoteData.NotAsked ->
            text ""

        RemoteData.Loading ->
            h3 [] [ text "Loading..." ]

        RemoteData.Success recipes ->
            viewRecipes (model, recipes)

        RemoteData.Failure httpError ->
            viewError (createErrorMessage httpError)

viewError : String -> Html Msg
viewError errorMessage =
    let
        errorHeading =
            "Couldn't fetch data at this time."
    in
        div []
            [ h3 [] [ text errorHeading ]
            , text ("Error: " ++ errorMessage)
            ]

viewRecipes : ( Model, List Recipe ) -> Html Msg
viewRecipes ( model, recipes )=
    div []
        [ hr [] []
        , h3 [] [ text ("Best recipes for your ingredients [" ++ model.firstIng ++ " " ++ model.secondIng ++ " " ++ model.thirdIng ++ " ] ordered by social ranking: " ) ]
        , table []
            ([ viewTableHeader ] ++ List.map viewRecipe recipes)
        ]

viewTableHeader : Html Msg
viewTableHeader =
    tr [] []

viewRecipe : Recipe -> Html Msg
viewRecipe recipe =
    tr []
        [ td []
            [ a [ href recipe.sourceUrl, target "_blank" ]  [ img [ src recipe.imageUrl, imgStyle, alt "Image can't be retrieved." ] [ text recipe.imageUrl ] ] ]
        , td []
            [ text recipe.title ]
        , td []
            [ text ( "Social Ranking: " ++ toString (round (recipe.socialRank ) ) ) ]
        ]

-- Msg

type Msg
    = SetFirstIngredient String
    | SetSecondIngredient String
    | SetThirdIngredient String
    | SendHttpRequest
    | DataReceived (WebData (List Recipe))

-- Decoder function(s)

recipeDecoder : Decoder Recipe
recipeDecoder =
    PL.decode Recipe
        |> PL.required "recipe_id" string
        |> PL.required "publisher" string
        |> PL.required "f2f_url" string
        |> PL.required "title" string
        |> PL.required "source_url" string
        |> PL.required "image_url" string
        |> PL.required "publisher_url" string
        |> PL.required "social_rank" float

-- Helpers

food2forkURL: String
food2forkURL =
    "https://www.food2fork.com/api/search?key=bbcbeb7e65a358ba3d95f12b786ce463&q="

buildQueryUrl : Model -> String
buildQueryUrl model =

      if String.isEmpty model.secondIng && String.isEmpty model.thirdIng
          then
              food2forkURL ++ model.firstIng
      else
          if String.isEmpty model.thirdIng
              then
                  food2forkURL ++ model.firstIng ++ Http.encodeUri "," ++ model.secondIng
          else
              food2forkURL ++ model.firstIng ++ Http.encodeUri "," ++ model.secondIng ++ Http.encodeUri "," ++ model.thirdIng

httpCommand : Model -> Cmd Msg
httpCommand model =

    (Json.Decode.field "recipes" (Json.Decode.list recipeDecoder))
        |> Http.get (buildQueryUrl model)
        |> RemoteData.sendRequest
        |> Cmd.map DataReceived

-- Update

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetFirstIngredient firstIng ->
            ( {model | firstIng = firstIng}, Cmd.none )

        SetSecondIngredient secondIng ->
            ( {model | secondIng = secondIng}, Cmd.none )

        SetThirdIngredient thirdIng ->
            ( {model | thirdIng = thirdIng}, Cmd.none )

        SendHttpRequest ->
            ( { model | recipes = RemoteData.Loading }, httpCommand model)

        DataReceived response ->
            ( { model | recipes = response }, Cmd.none )

createErrorMessage : Http.Error -> String
createErrorMessage httpError =
    case httpError of
        Http.BadUrl message ->
            message

        Http.Timeout ->
            "Server is taking too long to respond. Please try again later."

        Http.NetworkError ->
            "It appears you don't have an Internet connection right now."

        Http.BadStatus response ->
            response.status.message

        Http.BadPayload message response ->
            message

-- Init

init : ( Model, Cmd Msg )
init =
    ( { recipes = RemoteData.NotAsked
      , firstIng = ""
      , secondIng = ""
      , thirdIng = ""
      }
    , Cmd.none
    )

-- Main

main : Program Never Model Msg
main =
    program
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }

-- CSS Functions

headerContainerStyle : Attribute msg
headerContainerStyle =
    style
        [ ( "background-image", "url(Pics/spices.jpg)" )
        , ( "background-repeat", "no-repeat" )
        , ( "background-size", "cover" )
        , ( "height", "200px" )
        , ( "padding-left", "5cm" )
        , ( "padding-right", "5cm" )
        , ( "text-align", "center" )
        , ( "font-family", "sans-serif" )
        , ( "font-size", "30px" )
        , ( "color", "white" )
        ]

footerContainerStyle : Attribute msg
footerContainerStyle =
    style
        [ ( "display", "block" )
        , ( "align-items", "center" )
        , ( "justify-content", "center" )
        , ( "background-color", "#2F4F4F" ) --DarkSlateGrey
        , ( "background-size", "cover" )
        , ( "opacity", "0.8" )
        , ( "margin-top", "10px" )
        , ( "font-family", "sans-serif" )
        , ( "text-align", "center" )
        , ( "color", "white" )
        , ( "padding-left", "5cm" )
        , ( "padding-right", "5cm" )
        ]

formContainerStyle : Attribute msg
formContainerStyle =
    style
        [ ( "display", "flex" )
        , ( "align-items", "center" )
        , ( "justify-content", "space-around" )
        , ( "border-radius", "10px" )
        , ( "font-family", "sans-serif" )
        ]

formStyle : Attribute msg
formStyle =
    style
        [ ( "display", "flex" )
        , ( "align-items", "center" )
        , ( "justify-content", "space-around" )
        , ( "border-radius", "10px" )
        , ( "font-family", "sans-serif" )
        ]

buttonContainerStyle : Attribute msg
buttonContainerStyle =
    style
        [ ( "display", "flex" )
        , ( "justify-content", "center" )
        , ( "align-items", "center" )
        , ( "opacity", "0.75" )
        , ( "padding", "14px 20px" )
        , ( "margin-top", "10px" )
        , ( "border", "none" )
        , ( "border-radius", "10px" )
        , ( "font-size", "24px" )
        ]

buttonStyle : Attribute msg
buttonStyle =
    style
        [ ( "display", "inline-block" )
        , ( "background-color", "#e2725b" ) --soft red
        , ( "color", "#2F4F4F" )
        , ( "padding", "15px 32px" )
        , ( "border-radius", "10px" )
        , ( "text-decoration", "none" )
        , ( "font-size", "24px" )
        ]

imgStyle : Attribute msg
imgStyle =
    style
        [ ( "border-radius", "5px" )
        , ( "width", "200px" )
        , ( "height", "200px" )
        ]

firstfieldSetStyle : Attribute msg
firstfieldSetStyle =
    style
        [ ( "float", "left" )
        , ( "justify-content", "center" )
        , ( "background-image", "url(Pics/eggs.jpg)" )
        , ( "background-repeat", "no-repeat" )
        , ( "background-size", "cover" )
        , ( "background-color", "#e2725b" ) --soft red
        , ( "opacity", "0.75" )
        , ( "height", "250px" )
        , ( "margin", "10px 25px 10px 25px" )
        , ( "border-radius", "10px" )
        , ( "font-weight", "bold" )
        , ( "font-size", "24px" )
        , ( "color", "white" )
        ]

secondfieldSetStyle : Attribute msg
secondfieldSetStyle =
    style
        [ ( "float", "left" )
        , ( "justify-content", "center" )
        , ( "background-image", "url(Pics/tomatoes.jpg)" )
        , ( "background-repeat", "no-repeat" )
        , ( "background-size", "cover" )
        , ( "background-color", "#e2725b" ) --soft red
        , ( "opacity", "0.75" )
        , ( "height", "250px" )
        , ( "margin", "10px 25px 10px 25px" )
        , ( "border-radius", "10px" )
        , ( "font-weight", "bold" )
        , ( "font-size", "24px" )
        , ( "color", "white" )
        ]

thirdfieldSetStyle : Attribute msg
thirdfieldSetStyle =
    style
        [ ( "float", "left" )
        , ( "justify-content", "center" )
        , ( "background-image", "url(Pics/basil.jpg)" )
        , ( "background-repeat", "no-repeat" )
        , ( "background-size", "cover" )
        , ( "background-color", "#e2725b" ) --soft red
        , ( "opacity", "0.75" )
        , ( "height", "250px" )
        , ( "margin", "10px 25px 10px 25px" )
        , ( "border-radius", "10px" )
        , ( "font-weight", "bold" )
        , ( "font-size", "24px" )
        , ( "color", "white" )
        ]

explanationContainerStyle : Attribute msg
explanationContainerStyle =
    style
          [ ( "padding-left", "3cm" )
          , ( "padding-right", "3cm" )
          , ( "opacity", "0.75" )
          , ( "border-radius", "10px" )
          , ( "text-align", "center" )
          , ( "font-family", "sans-serif" )
          , ( "font-size", "24px" )
          , ( "color", "#2F4F4F" )
          ]

resultContainerStyle : Attribute msg
resultContainerStyle =
    style
          [ ( "display", "block" )
          , ( "align-items", "center" )
          , ( "justify-content", "center" )
          , ( "background-color", "#e2725b" ) --soft red
          , ( "margin", "0% 20%" )
          , ( "opacity", "0.75" )
          , ( "font-family", "sans-serif" )
          , ( "font-weight", "light" )
          , ( "text-align", "center" )
          , ( "color", "#2F4F4F" )
          ]

generalContainerStyle : Attribute Msg
generalContainerStyle =
    style
          [ ( "display", "block" )
          , ( "justify-content", "center" )
          , ( "align-items", "center" )
          , ( "background-color", "#e2725b" ) --soft red
          , ( "width", "auto" )
          , ( "height", "auto" )
          ]
